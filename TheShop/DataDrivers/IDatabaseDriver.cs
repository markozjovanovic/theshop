﻿using TheShop.Model;

namespace TheShop.DataDrivers
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDatabaseDriver
    {
        /// <summary>
        /// Return article with specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Article GetById(int id);

        /// <summary>
        /// Save passed in article
        /// </summary>
        /// <param name="article"></param>
        void Save(Article article);
    }
}
