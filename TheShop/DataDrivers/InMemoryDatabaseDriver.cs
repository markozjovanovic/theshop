﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheShop.Model;

namespace TheShop.DataDrivers
{
    /// <summary>
    /// In memory implementation of IDatabaseDriver
    /// </summary>
    public class InMemoryDatabaseDriver : IDatabaseDriver
    {
        private List<Article> _articles = new List<Article>();

        /// <summary>
        /// Return article with specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Article GetById(int id)
        {
            if (_articles.Any(x => x.ID == id))
            {
                return _articles.Single(x => x.ID == id);
            }
            throw new Exception("Could not find article with ID: " + id);
        }

        /// <summary>
        /// Save article
        /// </summary>
        /// <param name="article"></param>
        public void Save(Article article)
        {
            _articles.Add(article);
        }
    }
}
