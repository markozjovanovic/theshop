﻿using System;

namespace TheShop.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public class Logger : ILogger
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message)
        {
            Console.WriteLine("Info: " + message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            Console.WriteLine("Error: " + message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Debug(string message)
        {
            Console.WriteLine("Debug: " + message);
        }
    }
}
