﻿using System;
using TheShop.DataDrivers;
using TheShop.Logging;
using TheShop.Service;

namespace TheShop
{
	internal class Program
	{
		private static void Main(string[] args)
		{
            var shopService = new ShopService(new InMemoryDatabaseDriver(), new Logger());

            try
			{
				//order and sell
				shopService.OrderAndSellArticle(id: 1, maxExpectedPrice: 500, buyerId: 10);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}

			try
			{
				//print article on console
				var article = shopService.GetById(1);
				Console.WriteLine("Found article with ID: " + article.ID);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Article not found: " + ex);
			}

			try
			{
				//print article on console				
				var article = shopService.GetById(12);
				Console.WriteLine("Found article with ID: " + article.ID);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Article not found: " + ex);
			}

			Console.ReadKey();
		}
	}
}