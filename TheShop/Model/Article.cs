﻿using System;

namespace TheShop.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Article
    {
        public virtual int ID { get; set; }

        public string NameOfArticle { get; set; }

        public int ArticlePrice { get; set; }
        public bool IsSold { get; set; }

        public DateTime SoldDate { get; set; }
        public int BuyerUserId { get; set; }
    }
}
