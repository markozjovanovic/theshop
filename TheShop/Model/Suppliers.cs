﻿namespace TheShop.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class Supplier1 : AbstractSupplier
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Article GetArticle(int id)
        {
            return new Article()
            {
                ID = 1,
                NameOfArticle = "Article from supplier1",
                ArticlePrice = 458
            };
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Supplier2 : AbstractSupplier
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Article GetArticle(int id)
        {
            return new Article()
            {
                ID = 1,
                NameOfArticle = "Article from supplier2",
                ArticlePrice = 459
            };
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Supplier3 : AbstractSupplier
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Article GetArticle(int id)
        {
            return new Article()
            {
                ID = 1,
                NameOfArticle = "Article from supplier3",
                ArticlePrice = 460
            };
        }
    }
}
