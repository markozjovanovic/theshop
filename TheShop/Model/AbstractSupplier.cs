﻿namespace TheShop.Model
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractSupplier
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ArticleInInventory(int id)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        abstract public Article GetArticle(int id);
    }
}
