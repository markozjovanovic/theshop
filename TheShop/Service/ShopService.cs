﻿using System;
using System.Collections.Generic;
using TheShop.DataDrivers;
using TheShop.Logging;
using TheShop.Model;

namespace TheShop.Service
{
    /// <summary>
    /// 
    /// </summary>
	public class ShopService
	{
		private IDatabaseDriver databaseDriver;
		private ILogger logger;
        private IList<AbstractSupplier> suppliers;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="logger"></param>
        public ShopService(IDatabaseDriver driver, ILogger logger)
		{
			this.databaseDriver = driver;
			this.logger = logger;

            suppliers = new List<AbstractSupplier> {
                new Supplier1(),
                new Supplier2(),
                new Supplier3()
            };
        }

        /// <summary>
        /// Public method for ordering and selling articles
        /// </summary>
        /// <param name="id"></param>
        /// <param name="maxExpectedPrice"></param>
        /// <param name="buyerId"></param>
		public void OrderAndSellArticle(int id, int maxExpectedPrice, int buyerId)
		{
			var article = OrderArticle(id, maxExpectedPrice);

            if (article == null)
            {
                throw new Exception("Could not order article");
            }

            SellArticle(article, id, buyerId);
        }

        /// <summary>
        /// Returns Article with specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		public Article GetById(int id)
		{
			return databaseDriver.GetById(id);
		}
        
        /// <summary>
        /// Do order article if found
        /// </summary>
        /// <param name="id"></param>
        /// <param name="maxExpectedPrice"></param>
        /// <returns></returns>
        private Article OrderArticle(int id, int maxExpectedPrice)
        {
            Article article = null;
            Article tempArticle = null;

            #region ordering article
            foreach (var supplier in suppliers)
            {
                if (supplier.ArticleInInventory(id))
                {
                    tempArticle = supplier.GetArticle(id);
                    if (tempArticle.ArticlePrice < maxExpectedPrice)
                    {
                        article = tempArticle;
                    }
                }
            }
            #endregion

            return article;
        }

        /// <summary>
        /// Do sell article
        /// </summary>
        /// <param name="article"></param>
        /// <param name="id"></param>
        /// <param name="buyerId"></param>
        private void SellArticle(Article article, int id, int buyerId)
        {
            logger.Debug($"Trying to sell article with id= {id}");

            article.IsSold = true;
            article.SoldDate = DateTime.Now;
            article.BuyerUserId = buyerId;

            try
            {
                databaseDriver.Save(article);
                logger.Info($"Article with id={id} is sold.");
            }
            catch (ArgumentNullException ex)
            {
                logger.Error($"Could not save article with id={id}");
                throw new Exception("Could not save article with id");
            }
            catch (Exception)
            {
            }
        }
    }

}
