﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TheShop.DataDrivers;
using TheShop.Model;

namespace TheShop.Tests.DataDrivers
{
    [TestClass]
    public class InMemoryDatabaseDriverTest
    {
        /// <summary>
        /// Test if exception is thrown when article is not found
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.Exception), "Article not found.")]
        public void TestArticleNotFound()
        {
            InMemoryDatabaseDriver driver = new InMemoryDatabaseDriver();

            driver.GetById(0);
        }

        /// <summary>
        /// Test Save method
        /// </summary>
        [TestMethod]
        public void TestSaveArticle()
        {
            var id = 1;
            var articleMock = new Mock<Article>();
            articleMock.Setup(x => x.ID).Returns(id);
            InMemoryDatabaseDriver driver = new InMemoryDatabaseDriver();
            driver.Save(articleMock.Object);

            Article article = driver.GetById(id);

            Assert.AreEqual(article.ID, id);
        }
    }
}
