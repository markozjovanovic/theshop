﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheShop.Logging;

namespace TheShop.Tests.Logging
{
    [TestClass]
    public class LoggerTest
    {
        /// <summary>
        /// Test if Logger implements ILogger interface
        /// </summary>
        [TestMethod]
        public void TestIfLoggerImplementsILogger()
        {
            Assert.IsInstanceOfType(new Logger(), typeof(ILogger));
        }
    }
}
