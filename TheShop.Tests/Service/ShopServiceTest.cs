﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TheShop.DataDrivers;
using TheShop.Logging;
using TheShop.Model;
using TheShop.Service;

namespace TheShop.Tests.Service
{
    [TestClass]
    public class ShopServiceTest
    {
        /// <summary>
        /// Test if calling GetById method on shop service 
        /// calls method by the same name in database driver implementation
        /// </summary>
        [TestMethod]
        public void TestGetById()
        {
            int id = 0;
            var databaseDriverMock = new Mock<IDatabaseDriver>();
            var loggerMock = new Mock<ILogger>();
            ShopService shop = new ShopService(databaseDriverMock.Object, loggerMock.Object);

            shop.GetById(id);
            databaseDriverMock.Verify(m => m.GetById(id));
        }

        /// <summary>
        /// Article not found - too expensive
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(System.Exception), "Article not found - too expensive")]
        public void TestOrderAndSellArticleTooExpensive()
        {
            int id = 1, maxExpectedPrice = 1, buyerId = 10;
            var databaseDriverMock = new Mock<IDatabaseDriver>();
            var loggerMock = new Mock<ILogger>();
            ShopService shop = new ShopService(databaseDriverMock.Object, loggerMock.Object);

            shop.OrderAndSellArticle(id, maxExpectedPrice, buyerId);
        }

        /// <summary>
        /// Article found and saved
        /// </summary>
        [TestMethod]
        public void TestOrderAndSellArticle()
        {
            int id = 1, maxExpectedPrice = 500, buyerId = 10;
            var databaseDriverMock = new Mock<IDatabaseDriver>();
            var loggerMock = new Mock<ILogger>();
            ShopService shop = new ShopService(databaseDriverMock.Object, loggerMock.Object);

            shop.OrderAndSellArticle(id, maxExpectedPrice, buyerId);

            loggerMock.Verify(l => l.Debug(It.IsAny<string>()));
            loggerMock.Verify(l => l.Info(It.IsAny<string>()));

            databaseDriverMock.Verify(d => d.Save(It.IsAny<Article>()));
        }
    }
}
