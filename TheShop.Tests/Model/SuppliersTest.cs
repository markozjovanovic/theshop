﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TheShop.Model;

namespace TheShop.Tests.Model
{
    [TestClass]
    public class SuppliersTest
    {
        /// <summary>
        /// Test GetArticle method
        /// </summary>
        [TestMethod]
        public void TestGetArticle()
        {
            var supplier = new TestSupplier();

            Assert.IsInstanceOfType(supplier, typeof(AbstractSupplier));
            Assert.AreEqual(supplier.GetArticle(1).ID, 7);
        }
    }

    /// <summary>
    /// Supplier stub - abstract supplier implementation
    /// </summary>
    public class TestSupplier : AbstractSupplier
    {
        /// <summary>
        /// Get article method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override Article GetArticle(int id)
        {
            var articleMock = new Mock<Article>();
            articleMock.Setup(x => x.ID).Returns(7);

            return articleMock.Object;
        }
    }
}
